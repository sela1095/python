'''
#H.W 1 --- Write a function that adds 2 lists and returns a new list: [a,b,c,d] [1,2,3,4] -> [a,b,c,d,1,2,3,4]
def cat(list1, list2):
    newList = []
    newList = list1
    for ch in list2:
        newList.append(ch)
    return newList
ls1 = input('Enter first list separated by commas (a,b,c,d): \n').split(',')
ls2 = input('Enter second list separated by commas (0,1,2,3): \n').split(',')
print(cat(ls1,ls2))
'''

'''
#H.W 1 --- Write a function that merges 2 lists and returns a new list: [a,b,c,d] [1,2,3,4] -> [a,1,b,2,c,3,d,4]
#H.W 2
def cat(list1, list2):
    newList = []
    for i in range (0, list1.__len__()):
        newList.append(list1[i])
        newList.append(list2[i])
    return newList
ls1 = input('Enter first list separated by commas (a,b,c,d): \n').split(',')
ls2 = input('Enter second list with same amount of values separated by commas (0,1,2,3): \n').split(',')
print(cat(ls1,ls2))
'''

'''
Write a function that merges 2 sorted lists and returns 1 sorted list: [1,3,6] [2,4,5] -> [1,2,3,4,5,6]
def merge(xs, ys):
    zs = []
    i = 0
    j = 0
    while i < len(xs) and j < len(ys):
        if xs[i] < ys[j]:
            zs.append(xs[i])
            i += 1
        else:
            zs.append(ys[j])
            j += 1
    while i < len(xs):
            zs.append(xs[i])
            i += 1
    while j < len(ys):
        zs.append((ys[j]))
        j += 1
    return zs

print(merge(ls1,ls2))
'''

'''
# Selection sort function
def selection_sort(numbers: list[int]):
    for i in range(0, len(numbers)):
        tempMin = i
        for j in range(i, -1, -1):
            if numbers[i] < numbers[j]:
                tempMin = j
        numbers.insert(tempMin , numbers[i])
        numbers.pop(i+1)


if __name__ == '__main__':
    ns = [64, 25, 22, 12, 11]
    print(ns)
    selection_sort(ns)
    print(ns)
'''


'''
# Binary search function
from random import randrange
def binary_search(x: int, xs: list[int]) -> int:
    i = 0
    j = len(xs) - 1
    while i <= j:
        m = (i + j) // 2
        if xs[m] == x:
            return m
        elif xs[m] < x:
            i = m + 1
        else:
            j = m - 1
    return -1

if __name__ == '__main__':
    numbers = [randrange(0, 100) for k in range(50)]
    numbers.sort()
    print(numbers)
    print(binary_search(60, numbers))
'''

'''
# Add a frame to a list
def frame(ss: list[str]) -> list[str]:
    newlist = []
    maxlen = 0
    textlen = 0;
    for text in ss:
        maxlen = max(maxlen, len(text))

    newlist.append("*"*(maxlen+4));

    for text in ss:
        textlen = len(text)
        newlist.append(f"* {text}" + " "*(maxlen - textlen) + " *")
    newlist.append("*"*(maxlen+4));
    return newlist

if __name__ == '__main__':
    for line in frame(["the", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog"]):
        print(line)
'''


'''
# Get list from file, add a frame, and write to file
def frame(ss: list[str]) -> list[str]:
    newlist = []
    maxlen = 0
    textlen = 0;
    for text in ss:
        maxlen = max(maxlen, len(text))

    newlist.append("*"*(maxlen+4));

    for text in ss:
        textlen = len(text)
        newlist.append(f"* {text}" + " "*(maxlen - textlen) + " *")

    newlist.append("*"*(maxlen+4));
    return newlist

def file2list(filename) -> list[str]:
    newlist = []
    for text in filename.readlines():
        newtext: str = text
        newlist.append(newtext.replace('\n', ""))
    filename.close()
    return newlist

def list2file(ss: list[str]):
    file = open("D:/Sela/ron/newList.txt", "w")
    for text in ss:
        file.write(f"{text}\n")
    file.close()

if __name__ == '__main__':
    file = open("D:/Sela/ron/list.txt", "r")
    list2file(frame(file2list(file)))
'''

'''
from random import randrange
def histogram(xs: list[int]):
    for i in xs:
        hist = {}
        for i in xs:
            hist[i] = hist.get(i, 0) + 1
    return hist

numbers = [randrange(0, 100) for k in range(200)]
print(numbers)
print('number\titems')
hist = histogram(numbers)
for k in sorted(hist):
    print(f'{k}\t{hist[k]}')
'''
